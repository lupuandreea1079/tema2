function addTokens(input, tokens){
    if(typeof input!=='string'||input instanceof String){
    	throw "Invalid input";
    }
    else if(input.length<6){
    	throw "Input should have at least 6 characters";
    }
	  else if(!input.includes("...")){
    	return input;
    }
    else{
    	var words=input.split(" ");
    	var j=0;
    	for(var i=0;i<words.length;i++){
    		if(words[i].includes("...")){
    			input=input.replace(words[i], "${"+tokens[j].tokenName+"}");
    			j++;
    		}
    	}
    	return input;
    }
}


const app = {
    addTokens: addTokens
}

module.exports = app;

